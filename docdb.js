/**
 * Created by Pablo on 9/27/2016.
 */
"use strict";

var moment = require('moment');
var documentClient = require("documentdb").DocumentClient;
var config = require("./config");
var url = require('url');

var client = new documentClient(config.endpoint, {"masterKey": config.primaryKey});

var databaseUrl = `dbs/${config.database.id}`;
var collectionUrl = `${databaseUrl}/colls/${config.collection.id}`;

function queryCollection() {
  console.log(`Querying collection through index:\n${config.collection.id}`);

  return new Promise((resolve, reject) => {
    client.queryDocuments(
        collectionUrl,
        'SELECT * FROM root r'
    ).toArray((err, results) => {
      if (err) reject(err);
      else {
        console.info(`\tFound ${results.length} result(s)`);
        resolve(results);
      }
    });
  });
}

function convertEpochToISO8601(documentsToConvert) {
  for (var document of documentsToConvert) {
    if (moment(document.data.endTime).isValid()) {
      console.log(`\tDocument ${document.id} has a formatted value. It will not be converted.`);

    } else {
      console.log(`\tConverting document ${document.id} to ISO8601.`);

      var newTimeUtc = moment.utc(parseInt(document.data.endTime));
      document.data.endTimeEpoch = newTimeUtc.valueOf().toString();
      document.data.endTime = newTimeUtc.toJSON();

      // revert changes
//  document.data.startTime = "1468342441391"
//    document.data.startTime = document.data.startTimeEpoch; // 2016-08-04T12:55:15
//    delete document.data.startTimeEpoch;

      let documentUrl = `${collectionUrl}/docs/${document.id}`;

      client.replaceDocument(documentUrl, document, (err, result) => {
        if (err) console.error(`Error updating document ${result.id}. Error: ${JSON.stringify(err)}`);
        else {
          console.info(`Successfully updated document ${result.id}`);
        }
      });

/*
      return new Promise((resolve, reject) => {
        client.replaceDocument(documentUrl, document, (err, result) => {
          if (err) reject(err);
          else {
            resolve(result);
          }
        });
      });
*/
    }
  }
}

function exit(message) {
  console.log(message);
//  console.log('Press any key to exit');
//  process.stdin.setRawMode(true);
//  process.stdin.resume();
//  process.stdin.on('data', process.exit.bind(process, 0));
}

queryCollection()
    .then((queryResults) => convertEpochToISO8601(queryResults))
    .then(() => {
      exit('Completed successfully');
    })
    .catch((error) => {
      exit(`Completed with error ${JSON.stringify(error)}`)
    });