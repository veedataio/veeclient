/**
 * Created by Pablo on 9/27/2016.
 */
var config = {}

/** GetMoving */
config.endpoint = "https://getmoving.documents.azure.com:443/";
config.primaryKey = "m7MXVlcDsDM3ZyAQOyQLuzVKA9Skh3W0XK3OGFYn5tab49zaSFjTHfCjNQPL2YZFeGbNhnnqw9vfiSJIK8ovVA==";

/** Veeiot */
//config.endpoint = "https://veeiot.documents.azure.com:443/";
//config.primaryKey = "pCzgBmlFND2DhpKWYoagLlKrsfaaMoamVcaRIp8HICIWnAW40lcfD2bTuFGtPaiW1W1iEL3GygBGhP2cBCkxoA==";

config.database = {
  "id": "Training"
};

config.collection = {
  "id": "TrainingActivity"
};

module.exports = config;