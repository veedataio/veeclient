/**
 * Created by Pablo on 9/27/2016.
 */
"use strict";

var moment = require('moment');
var documentClient = require("documentdb").DocumentClient;
var config = require("./config");
var url = require('url');

var client = new documentClient(config.endpoint, { "masterKey": config.primaryKey });

var databaseUrl = "dbs/" + config.database.id;
var collectionUrl = databaseUrl + "/colls/" + config.collection.id;

function queryCollection() {
  console.log("Querying collection through index:\n" + config.collection.id);

  return new Promise(function (resolve, reject) {
    client.queryDocuments(collectionUrl, 'SELECT * FROM root r').toArray(function (err, results) {
      if (err) reject(err);else {
        console.info("\tFound " + results.length + " result(s)");
        resolve(results);
      }
    });
  });
}

function convertEpochToISO8601(documentsToConvert) {
  var _iteratorNormalCompletion = true;
  var _didIteratorError = false;
  var _iteratorError = undefined;

  try {
    for (var _iterator = documentsToConvert[Symbol.iterator](), _step; !(_iteratorNormalCompletion = (_step = _iterator.next()).done); _iteratorNormalCompletion = true) {
      var document = _step.value;

      if (moment(document.data.endTime).isValid()) {
        console.log("\tDocument " + document.id + " has a formatted value. It will not be converted.");
      } else {
        console.log("\tConverting document " + document.id + " to ISO8601.");

        var newTimeUtc = moment.utc(parseInt(document.data.endTime));
        document.data.endTimeEpoch = newTimeUtc.valueOf().toString();
        document.data.endTime = newTimeUtc.toJSON();

        // revert changes
        //  document.data.startTime = "1468342441391"
        //    document.data.startTime = document.data.startTimeEpoch; // 2016-08-04T12:55:15
        //    delete document.data.startTimeEpoch;

        var documentUrl = collectionUrl + "/docs/" + document.id;

        client.replaceDocument(documentUrl, document, function (err, result) {
          if (err) console.error("Error updating document " + result.id + ". Error: " + JSON.stringify(err));else {
            console.info("Successfully updated document " + result.id);
          }
        });

        /*
              return new Promise((resolve, reject) => {
                client.replaceDocument(documentUrl, document, (err, result) => {
                  if (err) reject(err);
                  else {
                    resolve(result);
                  }
                });
              });
        */
      }
    }
  } catch (err) {
    _didIteratorError = true;
    _iteratorError = err;
  } finally {
    try {
      if (!_iteratorNormalCompletion && _iterator.return) {
        _iterator.return();
      }
    } finally {
      if (_didIteratorError) {
        throw _iteratorError;
      }
    }
  }
}

function exit(message) {
  console.log(message);
  //  console.log('Press any key to exit');
  //  process.stdin.setRawMode(true);
  //  process.stdin.resume();
  //  process.stdin.on('data', process.exit.bind(process, 0));
}

queryCollection().then(function (queryResults) {
  return convertEpochToISO8601(queryResults);
}).then(function () {
  exit('Completed successfully');
}).catch(function (error) {
  exit("Completed with error " + JSON.stringify(error));
});

//# sourceMappingURL=docdb-compiled.js.map