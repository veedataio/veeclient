/**
 * Created by Pablo on 10/8/2016.
 */
"use strict";

var retry = require('async/retry');
var moment = require('moment');
var clientFromConnectionString = require('azure-iot-device-http').clientFromConnectionString;
var Message = require('azure-iot-device').Message;
var iothub = require('./iothub-utils');
var util = require('util');

function buildDeviceConnectionString(iothubConnString, deviceId) {
  return new Promise(function (resolve, reject) {
    iothub.getDeviceConnectionString(deviceId, iothubConnString, function (err, connString) {
      if (err) reject(err);else {
        resolve(connString);
      }
    });
  });
}

/*
 * Randomly generates heart rate messages and sends them to IoT hub.
 *
 * @param {integer} The number of messages to generate
 */
function sendHeartRateMessages(deviceId, connectionString, numberOfMessages) {
  var client = clientFromConnectionString(connectionString);

  var data = new Array(numberOfMessages);
  for (var i = 0; i < data.length; i++) {
    data[i] = {
      deviceId: deviceId,
      dataType: "sensor",
      dataSubtype: "heart-rate",
      data: {
        time_stamp: moment.utc().toJSON(),
        heart_rate: _generateRandomHeartRate(),
        quality: _generateHeartRateQuality()
      }
    };
  }

  var messages = [];
  data.forEach(function (value) {
    messages.push(new Message(JSON.stringify(value)));
  });

  console.log('Sending ' + messages.length + ' events in a batch to device ' + deviceId + ' => ' + JSON.stringify(data));

  return new Promise(function (resolve, reject) {
    client.sendEventBatch(messages, function (err, res) {
      if (err) reject(err);else {
        resolve(res);
      }
    });
  });
}

/*
 * Generates a random number between 44 and 122.
 * TODO: add weight to the generated numbers
 */
function _generateRandomHeartRate() {
  return Math.round(Math.random() * (122 - 44) + 44);
}

/*
 * Generates a random heart quality.
 */
function _generateHeartRateQuality() {
  var value = Math.round(Math.random());
  if (value === 1) return 'ACQUIRING';else return 'LOCKED';
}

function exit(message) {
  console.log(message);
  process.exit();
}

var iotHubConnectionString = 'HostName=Veeiot.azure-devices.net;SharedAccessKeyName=iothubowner;SharedAccessKey=aUlMy+pKCqtaRtRrd/H2nT+2k+4X7LBQtx2DOCTk6lg=';
var deviceIds = ['3875bd30-e3c3-4864-9dfd-f55fb44a8d6c'];

buildDeviceConnectionString(iotHubConnectionString, deviceIds[0]).then(function (deviceConnString) {
  return sendHeartRateMessages(deviceIds[0], deviceConnString, 5);
}).then(function () {
  exit('Completed successfully');
}).catch(function (error) {
  exit('Completed with error ' + JSON.stringify(error));
});

//# sourceMappingURL=mobile-compiled.js.map