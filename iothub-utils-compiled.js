/**
 * Created by Pablo on 10/9/2016.
 */
/**
 * Created by Pablo on 8/26/2016.
 */
'use strict';

var iotHub = require('azure-iothub');
var util = require('util');

module.exports = {
  getDeviceConnectionString: function getDeviceConnectionString(deviceId, iothubConnString, callback) {
    var registry = iotHub.Registry.fromConnectionString(iothubConnString);
    registry.get(deviceId, function (err, iotHubDevice, response) {
      if (err) {
        console.error(err);
        callback(err);
      } else {
        // RegEx for extracting 'HostName=' plus a URL from the Registry's connection string
        var regExp = /HostName=((?:[a-z|A-Z][a-z|A-Z.\d\-]+)\.(?:[a-z|A-Z][a-z|A-Z\-]+))(?![\w.])/g;
        var hostName = iothubConnString.match(regExp);

        // A connection string for a device follows the format "HostName=url;DeviceId=id;SharedAccessKey=abc"
        var connString = hostName + ';' + 'DeviceId=' + deviceId + ';' + _constructAuthenticationString(iotHubDevice.authentication);

        callback(null, connString);
      }
    });
  }
};

function _constructAuthenticationString(authenticationMechanism) {
  var authString = '';

  if (authenticationMechanism.SymmetricKey.primaryKey) {
    authString = 'SharedAccessKey=' + authenticationMechanism.SymmetricKey.primaryKey;
  } else if (authenticationMechanism.x509Thumbprint.primaryThumbprint || authenticationMechanism.x509Thumbprint.secondaryThumbprint) {
    authString = 'x509=true';
  }

  return authString;
}

//# sourceMappingURL=iothub-utils-compiled.js.map